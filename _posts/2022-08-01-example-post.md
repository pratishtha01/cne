---
layout: post
title: Test Post
date:   2022-08-01 10:40:00
tags: Experiments
subclass: 'post tag-Experiments'
categories: 'cne'
author: author_name
calendar: cne9967
email: pratishtha.tiwari@yahoo.com
navigation: True
logo: 'assets/images/home.png'
cover: 'assets/images/cover.png'
---

A simple post with details about the experiment and links to sign up can be created using this md file. Otherwise, the html guide file can also be used to create more comprehensive posts. 

