---
layout: page
title: About
class: 'post'
navigation: True
logo: 'assets/images/home1.png'
current: about
---

I. Le CNE

    1. L'équipe

    2. Collaborateurs scientifiques

    3. Histoire

    4. Le CNE en vidéos et en images

    5. Plan d'accès

    6. Contact

II. Axes de recherche

    1. Entraînement cognitif

    2. Interfaces cerveau-ordinateur

    3. Cerveau et alarmes

    4. Pattern oculaires

III. Publications

IV. Expériences 

    1. L'activité cérébrale et les jeux vidéos 

    2. Interactions hommes-robot 



